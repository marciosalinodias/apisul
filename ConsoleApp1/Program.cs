﻿using Core.Infraestrutura.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var elBO = new ElevadorBO();
            Console.WriteLine("Olá, seja bem vindo!");
            Console.WriteLine("\na. Qual é o andar menos utilizado pelos usuários:\r");
            Console.WriteLine(elBO.AndarMenosUtilizado());
            Console.WriteLine("\nb. Qual é o elevador mais frequentado e o período que se encontra maior fluxo:\r");
            Console.WriteLine(string.Concat(elBO.ElevadorMaisFrequentado(), " & ", elBO.PeriodoMaiorFluxo()));
            Console.WriteLine("\nc. Qual é o elevador menos frequentado e o período que se encontra menor fluxo:\r");
            Console.WriteLine(string.Concat(elBO.ElevadorMenosFrequentado(), " & ", elBO.PeriodoMenorFluxo()));
            Console.WriteLine("\nd. Qual o período de maior utilização do conjunto de elevadores:\r");
            Console.WriteLine(elBO.PeriodoMaiorUtilizacao());
            Console.WriteLine("\ne. Qual o percentual de uso de cada elevador com relação a todos os serviços prestados:\r");
            elBO.PercentualGeral().ToList().ForEach(item => Console.WriteLine(item));
            Console.ReadKey();
        }
    }
}

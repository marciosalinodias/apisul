﻿
using Core.DTO;
using Core.Infraestrutura.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Core.Infraestrutura.Repositories.ProvaAdmissionalCSharpApisul.Services
{
    public class ElevadorService : IElevadorService
    {
        public List<UsoElevador> ListaDados { get; set; }

        public ElevadorService(List<UsoElevador> ListaDados)
        {
            this.ListaDados = ListaDados;
        }

        private IList<dynamic> DadosAgrupadosCountAndar
        {
            get
            {
                return ListaDados.GroupBy(ue => ue.andar).Select(ue => new { ue.First().andar, count = ue.Count(), itens = ue.ToList() }).ToList<dynamic>();
            }
        }

        private IList<dynamic> DadosAgrupadosCountElevador
        {
            get
            {
                return ListaDados.GroupBy(ue => ue.elevador).Select(ue => new { ue.First().elevador, count = ue.Count(), itens = ue.ToList() }).ToList<dynamic>();
            }
        }

        private IList<dynamic> DadosAgrupadosCountTurno
        {
            get
            {
                return FiltraDadosTurno(ListaDados);
            }
        }

        private IList<dynamic> FiltraDadosTurno(List<UsoElevador> lista)
        {
            return lista.GroupBy(ue => ue.turno).Select(ue => new { ue.First().turno, count = ue.Count(), itens = ue.ToList() }).ToList<dynamic>();
        }

        private int GetCountElevador(char elevadorSelecionado)
        {
            var elevador = DadosAgrupadosCountElevador.First(ue => ((char)ue.elevador).Equals(elevadorSelecionado));
            return elevador != null ? elevador.count : 0;
        }

        private int GetPercentualElevador(char elevadorSelecionado)
        {
            var cont = GetCountElevador(elevadorSelecionado);
            return cont > 0 ? cont * 100 / ListaDados.Count : 0;
        }

        public List<int> andarMenosUtilizado()
        {
            return DadosAgrupadosCountAndar.OrderBy(ue => ue.count).Select(ue => (int)ue.andar).ToList();
        }

        public List<char> elevadorMaisFrequentado()
        {
            return DadosAgrupadosCountElevador.OrderByDescending(ue => ue.count).Select(ue => (char)ue.elevador).ToList();
        }

        public List<char> elevadorMenosFrequentado()
        {
            return DadosAgrupadosCountElevador.OrderBy(ue => ue.count).Select(ue => (char)ue.elevador).ToList();
        }

        public float percentualDeUsoElevadorA()
        {
            return GetPercentualElevador('A');
        }

        public float percentualDeUsoElevadorB()
        {
            return GetPercentualElevador('B');
        }

        public float percentualDeUsoElevadorC()
        {
            return GetPercentualElevador('C');
        }

        public float percentualDeUsoElevadorD()
        {
            return GetPercentualElevador('D');
        }

        public float percentualDeUsoElevadorE()
        {
            return GetPercentualElevador('E');
        }

        public List<char> periodoMaiorFluxoElevadorMaisFrequentado()
        {
            return FiltraDadosTurno((List<UsoElevador>)DadosAgrupadosCountElevador.First(ue => ((char)ue.elevador).Equals(elevadorMaisFrequentado().First())).itens).OrderByDescending(ue => (int)ue.count).Select(ue => (char)ue.turno).ToList();
        }

        public List<char> periodoMaiorUtilizacaoConjuntoElevadores()
        {
            return DadosAgrupadosCountTurno.OrderByDescending(ue => ue.count).Select(ue => (char)ue.turno).ToList();
        }

        public List<char> periodoMenorFluxoElevadorMenosFrequentado()
        {
            return FiltraDadosTurno((List<UsoElevador>)DadosAgrupadosCountElevador.First(ue => ((char)ue.elevador).Equals(elevadorMenosFrequentado().First())).itens).OrderBy(ue => (int)ue.count).Select(ue => (char)ue.turno).ToList();
        }
    }
}
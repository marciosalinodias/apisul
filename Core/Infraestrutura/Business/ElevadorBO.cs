﻿using Core.DTO;
using Core.Infraestrutura.DataAccess;
using Core.Infraestrutura.Repositories.ProvaAdmissionalCSharpApisul.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infraestrutura.Business
{
    public class ElevadorBO
    {
        private ElevadorService _Service;
        
        public ElevadorService Service
        {
            get {
                return _Service;
            }
        }

        public ElevadorBO()
        {
            _Service = new ElevadorService(DataAccessService.LoadJson());
        }

        public int AndarMenosUtilizado()
        {
            return _Service.andarMenosUtilizado().First();
        }

        public char ElevadorMaisFrequentado()
        {
            return _Service.elevadorMaisFrequentado().First();
        }

        public char PeriodoMaiorFluxo()
        {
            return _Service.periodoMaiorFluxoElevadorMaisFrequentado().First();
        }

        public char ElevadorMenosFrequentado()
        {
            return _Service.elevadorMenosFrequentado().First();
        }

        public char PeriodoMenorFluxo()
        {
            return _Service.periodoMenorFluxoElevadorMenosFrequentado().First();
        }

        public char PeriodoMaiorUtilizacao()
        {
            return _Service.periodoMaiorUtilizacaoConjuntoElevadores().First();
        }

        public string[] PercentualGeral()
        {
            return new string[]{
                string.Concat("A -> ", _Service.percentualDeUsoElevadorA(), "% \r"),
                string.Concat("B -> ", _Service.percentualDeUsoElevadorB(), "% \r"),
                string.Concat("C -> ", _Service.percentualDeUsoElevadorC(), "% \r"),
                string.Concat("D -> ", _Service.percentualDeUsoElevadorD(), "% \r"),
                string.Concat("E -> ", _Service.percentualDeUsoElevadorE(), "% \r"),
            };
        }

    }
}

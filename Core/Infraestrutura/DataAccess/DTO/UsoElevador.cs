﻿using Core.Infraestrutura.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.DTO
{
    public class UsoElevador
    {
        public int andar { get; set; }
        public char elevador { get; set; }
        public char turno { get; set; }

    }
}

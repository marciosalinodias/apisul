﻿using Core.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infraestrutura.DataAccess
{
    internal class DataAccessService
    {
        public static List<UsoElevador> LoadJson()
        {
            return JsonConvert.DeserializeObject<List<UsoElevador>>(File.ReadAllText(@"./Data/input.json"));
        }
    }
}
